<?php

namespace App\Serializer;

use App\DTO\GameDTO;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class GameSerializer
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function fromApiResponse(array $data): array
{
    $games = [];

    foreach ($data as $item) {
        $gameDto = new GameDTO();
        $gameDto->setId($item['id']);
        $gameDto->setTitle($item['title']);
        $gameDto->setThumbnail($item['thumbnail']);
        $gameDto->setShortDescription($item['short_description']);
        $gameDto->setGameUrl($item['game_url']);
        $gameDto->setGenre($item['genre']);
        $gameDto->setPlatform($item['platform']);
        $gameDto->setPublisher($item['publisher']);
        $gameDto->setDeveloper($item['developer']);
        $gameDto->setReleaseDate($item['release_date']);
        $gameDto->setFreetogameProfileUrl($item['freetogame_profile_url']);

        $games[] = $gameDto;
    }

    return $games;
}

}
