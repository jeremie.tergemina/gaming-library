<?php

namespace App\Controller;

use App\Entity\Game;
use App\Message\GameMessage;
use App\Repository\GameRepository;
use App\Service\GameApiService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GameController extends AbstractController
{
    private $gameApiService;

    private PaginatorInterface $paginator;

    private $EntityManager;

    private $gameRepository;

    function __construct(GameApiService $gameApiService, PaginatorInterface $paginator, EntityManagerInterface $EntityManager, GameRepository $gameRepository)
    {
        $this->gameApiService = $gameApiService;
        $this->paginator = $paginator;
        $this->EntityManager = $EntityManager;
        $this->gameRepository = $gameRepository;
    }
    #[Route('/game', name: 'app_gamelist')]
    public function GameListAction(Request $request): Response
    {
        $allGames = $this->gameApiService->getGames();

        // Filter games by search query
        $searchQuery = $request->query->get('search', '');
        $filteredGames = array_filter($allGames, function ($game) use ($searchQuery) {
            return stripos($game->getTitle(), $searchQuery) !== false;
        });

        $pagination = $this->paginator->paginate(
            $filteredGames,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('game/gamelist.html.twig', [
            'pagination' => $pagination
        ]);
    }

    #[Route('/game/{id}', name: 'app_gamedetails')]
    public function GameDetailsAction(int $id, Request $request): Response
    {
        $games = $this->gameApiService->getGames();
        $game = null;
    
        foreach ($games as $dto) {
            if ($dto->getId() === $id) {
                $game = $dto;
                break;
            }
        }

        if ($request->isMethod('POST')) {
            $content = $request->request->get('message_content');
            $game_id = $request->request->get('game_id');
            $user = $this->getUser();
    
            if (!$user) {
                // You might want to handle this case differently depending on your application
                throw $this->createAccessDeniedException();
            }
    
            $message = new Game();
            $message->setGameId($game_id);
            $message->setText($content);
            $message->setCreatedAt(new \DateTime());
            $message->setCreatedBy($user);

            $this->EntityManager->persist($message);
            $this->EntityManager->flush();
    
            // Redirect to the same page to prevent form resubmission on refresh
            return $this->redirectToRoute('app_gamedetails', ['id' => $id]);
        }
    
        if ($game === null) {
            throw $this->createNotFoundException(sprintf('Game with id %d not found', $id));
        }
        $messages = $this->gameRepository->findBy(['gameId' => $game->getId()], ['createdAt' => 'DESC']);
    
        return $this->render('game/gamedetails.html.twig', [
            'game' => $game,
            'messages' => $messages,
        ]);
    }
}
