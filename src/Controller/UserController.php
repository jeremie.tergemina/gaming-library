<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{
    private $entityManager;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="app_index")
     */
    public function indexAction() : Response
    {
        if($this->getUser()) {
            return $this->redirectToRoute('app_gamelist');
        } else {
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/profile", name="app_edit_profile")
     */
    public function editProfile(Request $request, UserPasswordHasherInterface $passwordManager, UserRepository $repository): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            if ($form->get('new_password')->getData()) {
                $user->setPassword($passwordManager->hashPassword($user, $form->get('new_password')->getData()));
            }

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'Your profile has been updated!');

            return $this->redirectToRoute('app_edit_profile');
        }

        return $this->render('profile/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
