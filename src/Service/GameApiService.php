<?php

namespace App\Service;

use GuzzleHttp\Client;
use App\Serializer\GameSerializer;

class GameApiService
{
    private $client;

    private GameSerializer $gameSerializer;


    public function __construct(GameSerializer $gameSerializer)
    {
        $this->client = new Client([
            'base_uri' => 'https://free-to-play-games-database.p.rapidapi.com/api/',
            'headers' => [
                'X-RapidAPI-Key' => '7a8f0a0e71msh0e7b6644496df46p15454bjsn2f2c71a49a68',
                'X-RapidAPI-Host' => 'free-to-play-games-database.p.rapidapi.com',
            ],
        ]);
        $this->gameSerializer = $gameSerializer;
    }

    public function getGames(array $params = []): array
    {
        $response = $this->client->request('GET', 'games', ['query' => $params]);
        $data = json_decode($response->getBody()->getContents(), true);

        return $this->gameSerializer->fromApiResponse($data);
    }
}
