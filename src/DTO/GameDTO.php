<?php

namespace App\DTO;

class GameDTO
{
    
    public int $id;

    public string $title;

    public string $thumbnail;

    public string $shortDescription;

    public string $gameUrl;

    public string $genre;

    public string $platform;

    public string $publisher;

    public string $developer;

    public string $releaseDate;

    public string $freeToGameProfileUrl;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): void
    {
        $this->thumbnail = $thumbnail;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    public function getGameUrl(): string
    {
        return $this->gameUrl;
    }

    public function setGameUrl(string $gameUrl): void
    {
        $this->gameUrl = $gameUrl;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): void
    {
        $this->genre = $genre;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): void
    {
        $this->platform = $platform;
    }

    public function getPublisher(): string
    {
        return $this->publisher;
    }

    public function setPublisher(string $publisher): void
    {
        $this->publisher = $publisher;
    }

    public function getDeveloper(): string
    {
        return $this->developer;
    }

    public function setDeveloper(string $developer): void
    {
        $this->developer = $developer;
    }

    public function getReleaseDate(): string
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(string $releaseDate): void
    {
        $this->releaseDate = $releaseDate;
    }

    public function getFreeToGameProfileUrl(): string
    {
        return $this->freeToGameProfileUrl;
    }

    public function setFreeToGameProfileUrl(string $freeToGameProfileUrl): void
    {
        $this->freeToGameProfileUrl = $freeToGameProfileUrl;
    }
}
