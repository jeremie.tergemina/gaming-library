# Gaming Library


## Description

Gaming Library est une librairie de jeux f2p ou on peut discuter avec ces amis pour debattre directement sur la description du jeu

## Prerequis

- PHP 8 ou version supérieure
- MySQL 5.7 ou version supérieure
- Composer
- Node.js
- npm
- Symfony CLI

## Installation

Clonez le projet : git clone https://gitlab.com/jeremie.tergemina/gaming-library.git

- Conteneur symfony

Installez les dépendances PHP, pour ça il va falloir lancer le container php : docker exec -it symfony_project-symfony-1
Copiez le fichier .env.dist en .env et modifiez les valeurs des variables d'environnement si nécessaire : cp .env.dist .env
Créez la base de données : php bin/console doctrine:database:create
Créer une migration : php bin/console make:migration
Appliquez les migrations : php bin/console doctrine:migrations:migrate
Appliquez les fixtures : php bin/console doctrine:fixtures:load --append
Quittez le conteneur php 

- base de données

s'il y a des probleme au niveau de la bdd, il faut que vous suivez ces étapes : 
ouvrez le conteneur db : docker exec -it symfony_project-db-1
tapez la commande : mysql root -u
le mot de passe : my-secret-pw
et enfin tapez les 2 commandes qui sont dans db/permissions.sql

- Frontend

Installez les dépendances front-end : npm install
Compilez les assets : et si vous toucher au scss il faut lancer la commande du webpack pour qu'il compile : symfony run -d npm run watch

faites un docker-compose up -d --build pour vous lancer

## Environnement


plus qu'a : Docker-compose up -d

## Contribuer
Les contributions sont les bienvenues ! Pour contribuer, suivez ces étapes :

Forker le projet
Créez une branche (git checkout -b ma-branche)
Committez vos modifications (git commit -am 'Ajout de nouvelles fonctionnalités')
Pusher votre branche (git push origin ma-branche)
Créez une nouvelle Pull Request

## Licence
Ce projet est sous licence MIT. Voir le fichier LICENSE pour plus d'informations.